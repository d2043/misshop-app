# Misshop App

This project was created for the purpose of helping customers to register information such as changing addresses, registering number plates, ... more easily.

## Prerequisites

- [Node.js > 12](https://nodejs.org) and npm  or yarn  (Recommended: Use [nvm](https://github.com/nvm-sh/nvm))
- [Watchman](https://facebook.github.io/watchman)
- [Xcode 12](https://developer.apple.com/xcode)
- [Cocoapods](https://cocoapods.org)
- [JDK > 11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)
- [Android Studio and Android SDK](https://developer.android.com/studio)

## Base dependencies

- [axios](https://github.com/axios/axios) for networking.
- [typescript](https://www.typescriptlang.org/) to type-check.
- [react-navigation](https://reactnavigation.org/) navigation library.
- [async-storage](https://github.com/react-native-async-storage/async-storage) as storage solution.
- [redux](https://redux.js.org/) for state management.
- [redux-thunk](https://github.com/gaearon/redux-thunk) to dispatch asynchronous actions.
- [react-native-signature-canvas](https://github.com/YanYuanFE/react-native-signature-canvas) to write signature.

## Usage

### Using this project

Keep in mind that this library can cause trouble if you are renaming a project that uses `Pods` on the iOS side.

- Go to your project's root folder and run

  ```markdown
  npm install
  ```

  or

  ```markdown
  yarn install
  ```

- Run `npm run ios` or `npm run android` to start your application!

(Using yarn: `yarn ios` or `yarn android`)

Note: Please read the Setup environments section that is below in this file for more information about the execution scripts.

## Folder structure

```
AutoGouv
├── assets
│   ├── fonts
│   ├── images
├── src/app
│   ├── components
│   ├── hooks
│   ├── navigation
│   ├── redux
│   ├── screens
│   ├── styles
│   ├── locate
│   ├── types
│   ├── utils
├── README.md
├── android
│   ├── app
│   ├── build.gradle
│   ├── gradle
│   ├── gradle.properties
│   ├── gradlew
│   ├── gradlew.bat
│   ├── keystores
│   └── settings.gradle
├── App.tsx
├── index.js
├── ios
│   ├── Autogouv
│   ├── Autogouv-tvOS
│   ├── Autogouv-tvOSTests
│   ├── Autogouv.xcodeproj
│   └── AutogouvTests
└── package.json
```

This template follows a very simple project structure:

- `src`: This folder is the main container of all the code inside your application.
  - `assets`: Asset folder to store all images, vectors, etc.
  - `components`: Folder to store any common component that you use through your app (such as a generic button)
  - `hooks`: Folder to store any custom hooks for app.
  - `utils`: Folder to store all your network logic (you should have one controller per resource),common function.
  - `locate`: Folder to store the languages files.
  - `navigation`: Folder to store the navigators.
  - `screens`: Folder that contains all your application screens/features.
  - `styles`: Folder to store all the styling concerns related to the application theme.
  - `redux`: Folder to store all logic for state management.
  - `App.tsx`: Main component that starts your whole app.
  - `index.js`: Entry point of your application as per React-Native standards.
