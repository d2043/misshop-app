import moment from 'moment';

export const getDateDisplay = (date?: string, isNotShowTime?: boolean) => {
	if (!date) {
		return '';
	}
	const [listDate, listTime] = date.split(' ');

	const [year, month, day] = listDate.split('-');
	const [hours, minutes] = listTime.split(':');

	const timeString = `${hours}:${minutes} `;
	return `${isNotShowTime ? '' : timeString}${day}-${month}-${year}`;
};
export const getToday = new Date(moment().utc().add(1, 'hour').toString());
export const getTomorrow = (() => {
	const tomorrow = new Date(moment().utc().add(1, 'days').add(1, 'hours').toString());
	return tomorrow;
})();
export const getHour = moment().utc().add(1, 'hours').hour();
