import Button from 'app/components/Button';
import TextBoxInput from 'app/components/group/TextboxInput';
import { useForm } from 'app/components/hooks/useForm';
import { IconEye } from 'app/components/icons/Icons';
import { getTranslate } from 'app/locate/reducer';
import { useAppDispatch, useAppSelector } from 'app/redux/store/hooks';
import { screenHeight, screenWidth } from 'app/styles/dimens';
import { AreaContainer, Container, KeyboardContainer, PADDING_CONTAINER } from 'app/styles/globalStyled';
import styled from 'app/styles/styled';
import React, { useCallback, useEffect, useState } from 'react';
import { popNavigate } from 'app/navigation/rootNavigation';
import { getRegisterState, registerAuth, resetState } from './reducer';
import { showModal } from 'app/components/modal/reducer';
import { store } from 'app/redux/store';

const Register = () => {
  const getString = getTranslate();
  const dispatch = useAppDispatch();
  const statusRegister = useAppSelector(getRegisterState);
  // const user = useAppSelector(getUser);
  const [changeSecurePassword, setChangeSecurePassword] = useState(true);

  useEffect(() => {
    if (statusRegister !== 'success') return;
    store.dispatch(resetState())
    setTimeout(() => {
      dispatch(
        showModal({
          status: 'SUCCESS',
          title: 'Register',
          message: 'Register successful!',
          positiveButton: { onPress: () => popNavigate() },
        })
      );
    }, 500);
  }, [statusRegister]);

  const { handleSubmit, handleChange, data, errors } = useForm<{ userName: string; email: string; password: string; brandName: string; address: string; code: string; phoneNumber: string; status: string }>({
    validations: {
      email: {
        required: getString('Register', 'EmailRequire'),
      },
      userName: {
        required: getString('Register', 'UserNameRequire'),
      },
      password: {
        required: getString('Login', 'PasswordRequire'),
      },
      brandName: {
        required: getString('Register', 'BrandNameRequire'),
      },
      code: {
        required: getString('Register', 'CodeRequire'),
      }
    },
  });

  const onPressSecurePassword = useCallback(() => {
    setChangeSecurePassword(!changeSecurePassword);
  }, [changeSecurePassword]);

  const onPressHaveAccount = () => {
    popNavigate()
  };

  const onPressRegister = () => {
    if (!Object.keys(handleSubmit()).length) {
      dispatch(
        registerAuth({
          user_name: data.userName,
          password: data.password,
          email: data.email,
          brand_name: data.brandName,
          address: data.address,
          code: data.code,
          phone_number: data.phoneNumber,
          status: 'active'
        })
      );
    }
  };

  return (
    <KeyboardContainer notPadding scrollEnabled={false}>
      <ParentContainer>
        <ContainerLogin notPadding>
          <ContainerImage>
            <ImageHeader source={require('images/logo_app.png')} />
          </ContainerImage>

          <TextBoxInput
            placeholder={getString('Register', 'Username')}
            value={data.userName as string}
            handleChange={handleChange('userName')}
            error={errors.userName}
          />
          <PasswordView
            secureTextEntry={changeSecurePassword}
            value={data.password as string}
            placeholder={getString('Login', 'Password')}
            handleChange={handleChange('password')}
            error={errors.password}
          >
            <EyePassword isPress={changeSecurePassword} onPress={onPressSecurePassword} />
          </PasswordView>
          <TextBoxInput
            placeholder={getString('Register', 'Email')}
            value={data.email as string}
            handleChange={handleChange('email')}
            error={errors.email}
          />
          <TextBoxInput
            placeholder={getString('Register', 'PhoneNumber')}
            value={data.phoneNumber as string}
            handleChange={handleChange('phoneNumber')}
          />
          <TextBoxInput
            placeholder={getString('Register', 'BrandName')}
            value={data.brandName as string}
            handleChange={handleChange('brandName')}
            error={errors.brandName}
          />
          <TextBoxInput
            placeholder={getString('Register', 'Code')}
            value={data.code as string}
            handleChange={handleChange('code')}
            error={errors.code}
          />
          <TextBoxInput
            placeholder={getString('Register', 'Address')}
            value={data.address as string}
            handleChange={handleChange('address')}
            error={
              statusRegister === 'existed'
                ? getString('Register', 'RegisterExisted')
                : statusRegister === 'failed'
                  ? getString('Register', 'RegisterFailed')
                  : ''
            }
          />
          <LoginBottom>
            <CustomButton
              // loading={statusLogin === 'loading'}
              children={getString('Login', 'SignUp')}
              onPress={onPressRegister}
            />
          </LoginBottom>

          <FingerTouchOpacity onPress={onPressHaveAccount}>
            <TextCaption>
              {getString('Login', 'HaveAccount')}
              <TextOrange>
                {getString('Login', 'LoginHere')}
              </TextOrange>
            </TextCaption>
          </FingerTouchOpacity>
        </ContainerLogin>
      </ParentContainer>
    </KeyboardContainer>
  );
};
const ContainerLogin = styled(AreaContainer)`
	background-color: ${({ theme }) => theme.colors.background};
	align-items: center;
	justify-content: center;
`;
const ParentContainer = styled(Container)`
	background-color: ${({ theme }) => theme.colors.background};
	height: ${screenHeight}px;
`;
const BodyTop = styled.View`
	justify-content: space-around;
	margin-bottom: 24px;
`;
const LoginBottom = styled.View`
	align-self: center;
`;
const EyePassword = styled(IconEye)`
	position: absolute;
	width: 100%;
	right: ${({ theme }) => theme.scapingElement};
`;
const ContainerImage = styled.View`
	width: ${screenWidth - PADDING_CONTAINER * 3}px;
	height: ${screenHeight * 0.2}px;
`;
const ImageHeader = styled.Image`
	width: 100%;
	height: 100%;
	resize-mode: contain;
`;

const TextOrange = styled.Text`
	color: ${({ theme }) => theme.colors.orange_100};
	font-weight: bold;
`;
const PasswordView = styled(TextBoxInput)`
	justify-content: center;
`;
const TextCaption = styled.Text`
	font-size: ${({ theme }) => theme.font.fontMedium};
	color: ${({ theme }) => theme.colors.text};
	margin: 6px 0px;
`;
const FingerTouchOpacity = styled.TouchableOpacity`
	margin-top: ${({ theme }) => theme.scapingElement};
	flex-direction: row;
	align-items: center;
	align-self: center;
`;
const CustomButton = styled(Button)`
	width: ${screenWidth * 0.6}px;
	margin-top: ${({ theme }) => theme.scapingElement};
`;
export default Register;
