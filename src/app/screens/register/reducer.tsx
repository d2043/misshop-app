import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from 'app/redux/store';
import { query } from 'app/utils/api';
import { State } from 'react-native-gesture-handler';

let initState: {
  status?: Status | 'existed';
} = {
  status: 'none',
};

interface RegisterType {
  user_name: string;
  password: string;
  email: string;
  brand_name: string;
  address: string;
  code: string;
  phone_number: string;
  status: string;
}

export const registerAuth = createAsyncThunk(
  'register/registerAuth',
  async (userInfo: RegisterType) => {
    const res = await query<RegisterResponse, RegisterType>('/register', 'POST', userInfo);
    return res;
  }
)

const RegisterSlice = createSlice({
  initialState: initState,
  name: 'register',
  reducers: {
    resetState: (state) => {
      state.status = 'none'
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(registerAuth.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(registerAuth.fulfilled, (state, action: PayloadAction<RegisterResponse | undefined>) => {
        const response = action.payload;
        state.status = response?.status === 'OK'
          ? 'success'
          : response?.errors === 'EXISTED'
            ? 'existed'
            : 'failed'
      });
  }
});

export const { resetState } = RegisterSlice.actions;

export const getRegisterState = (state: RootState) => state.register.status;

const registerReducer = RegisterSlice.reducer;
export default registerReducer;